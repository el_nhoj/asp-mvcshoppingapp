﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PartB.Models
{
    // Custom age validation data annotation
    // Adapted and modified from 
    // http://www.aubrett.com/InformationTechnology/WebDevelopment/MVC/DataAnnotationBirthdate.aspx
    public class DOBCheckAttribute : ValidationAttribute
    {
        public DOBCheckAttribute(int minAge)
        {
            this.minAge = minAge;    
        }

        public int minAge { get; set; }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                int age = DateTime.Now.Year - ((DateTime)value).Year;

                return (age >= minAge) ? true : false;
            }
            else 
            {
                return false;
            }
        }
    }
}