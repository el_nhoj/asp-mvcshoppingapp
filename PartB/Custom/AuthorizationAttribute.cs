﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PartB.Custom
{
    // Custom authorization attribute sourced from Week 8 example code
    public class AuthorizationAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Session["Email"] != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                                   new RouteValueDictionary(new { controller = "Customers", action = "Login" }));
        }
    }
}