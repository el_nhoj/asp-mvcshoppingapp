﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartB.Models;
using PartB.Custom;

namespace PartB.Controllers
{
    public class CheckoutController : Controller
    {
        private StoreEntities se = new StoreEntities();

        // GET: Checkout        
        public ActionResult Index()
        {
            return RedirectToAction("OrderCheckout");                           
        }

        [Authorization]
        public ActionResult OrderCheckout()
        {
            Cart cart = Session["Cart"] as Cart;

            // Check if cart has any items in it
            if (cart.cartItems.Any())
            {
                Session["Cart"] = cart;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Cart");
            }            
        }

        // Method to create an order object and write to database
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderCheckout([Bind(Include = "fullName,streetAddress,city,state,postcode")] Order order) 
        {
            if (ModelState.IsValid)
            {                              
                Cart cart = Session["Cart"] as Cart;

                // Copy cart item details to create order items
                foreach (var item in cart.cartItems)
                {
                    var orderItem = new OrderItem()
                    {
                        orderID = order.orderID,
                        productName = item.product.Title,
                        productQty = item.quantity,
                        productSubtotal = item.subtotal
                    };
                    
                    se.OrderItems.Add(orderItem);
                }

                // Fill in rest of order details
                order.totalPrice = cart.total;
                order.orderDate = DateTime.Now;
                order.customerID = Convert.ToInt32(Session["ID"]);

                se.Orders.Add(order);
                se.SaveChanges();

                // Empty cart
                cart.cartItems.Clear();
                Session["Cart"] = cart;

                return RedirectToAction("OrderSuccessful", new { id = order.orderID, cid = order.customerID});
            }
            return View(order);
        }

        public ActionResult OrderSuccessful(int id, int cid) 
        {
            int v = Convert.ToInt32(Session["ID"]);
            
            // Check if customer session is the same, redirect if not
            if (v == cid)
            {
                // Return the order
                var order = se.Orders.FirstOrDefault(o => o.orderID == id);

                var match = from a in se.OrderItems
                            where a.orderID == id
                            select a;

                // Find relevant items of the order and remove if duplicates exist
                if (match != null) 
                {
                    foreach (var item in match) 
                    {
                        if (!order.itemList.Contains(item))
                            order.itemList.Add(item);
                    }
                }
                
                return View(order);
            }
            else 
            {
                return RedirectToAction("Index", "Home");
            } 
        }
    }
}