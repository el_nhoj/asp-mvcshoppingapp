﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PartB.Models;
using PartB.Custom;

namespace PartB.Controllers
{
    public class ProductsController : Controller
    {
        private s3401071Entities db = new s3401071Entities();

        // GET: Products
        [Authorization]
        public ActionResult Index(string productCategory, string searchString)
        {
            // Search method sourced from Week 9 example code
            var CategoryList = new List<string>();

            var CatQuery = from d in db.Products
                            orderby d.Category.Title
                            select d.Category.Title;

            CategoryList.AddRange(CatQuery.Distinct());
            ViewBag.productCategory = new SelectList(CategoryList);

            var products = from p in db.Products
                           select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(q => q.Title.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(productCategory))
            {
                products = products.Where(r => r.Category.Title == productCategory);
            }

            return View(products);
        }
       
        // GET: Products/Details/5
        [Authorization]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
    }
}
