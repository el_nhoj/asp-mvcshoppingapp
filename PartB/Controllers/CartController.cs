﻿using PartB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartB.Custom;

namespace PartB.Controllers
{
    public class CartController : Controller
    {
        private s3401071Entities db = new s3401071Entities();
        private StoreEntities se = new StoreEntities();
        
        // GET: Cart
        // Cart paging feature take from Week 9 example code with NuGet PagedList and PagedList.Mvc packages
        [Authorization]
        public ActionResult Index(int pageNumber = 1)
        {
            const int PageSize = 3;
            
            Cart cart = Session["Cart"] as Cart;

            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = PageSize;
            ViewBag.TotalProducts = cart.cartItems.Count();           
            
            cart.total = 0;

            // Calculates the cart's total
            foreach (CartItem c in cart.cartItems) 
            {
                c.subtotal = (c.quantity * c.product.Price);
                cart.total += c.subtotal;
            }

            return View(cart);
        }

        // Method to add to cart
        public ActionResult AddToCart(int id, string qty) 
        {
            int num;
            bool isNumber = int.TryParse(qty, out num);

            // Check if qty string is a number and if it is greater than 0
            if (isNumber && num > 0)
            {
                // Lookup product for matching ID and return object
                var product = db.Products.FirstOrDefault(p => p.ProductID == id);

                // Set a cart as casted Session["Cart"] 
                Cart cart = Session["Cart"] as Cart;

                // Lookup CartItem to see if it exists in Cart's list
                CartItem match = cart.cartItems.FirstOrDefault(q => q.product.ProductID == product.ProductID);

                // If match is found, update the CartItem's quantity, otherwise add new object
                if (match != null)
                {
                    match.quantity += num;
                }
                else
                {
                    cart.cartItems.Add(new CartItem(product, num));
                }

                Session["Cart"] = cart;

                return RedirectToAction("Index", "Cart");
            }
            else 
            {
                TempData["error"] = "Please enter an valid product quantity.";
                return RedirectToAction("Index", "Products");
            } 
            
        }

        // Method to remove from cart
        public ActionResult RemoveFromCart(int id)
        {
            // Set a cart as casted Session["Cart"]
            Cart cart = Session["Cart"] as Cart;

            // Lookup CartItem to see if it exists in Cart's list
            CartItem match = cart.cartItems.FirstOrDefault(q => q.product.ProductID == id);

            if (match != null) 
            { 
                cart.cartItems.Remove(match);
            }

            Session["Cart"] = cart;
            
            return RedirectToAction("Index", "Cart");
        }
 
    }
}