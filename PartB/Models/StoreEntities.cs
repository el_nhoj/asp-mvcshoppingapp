﻿using System.Data.Entity;

namespace PartB.Models
{
    public class StoreEntities : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
    }
}