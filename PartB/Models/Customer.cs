﻿using System;
using System.ComponentModel.DataAnnotations;
using PartB.Custom;

namespace PartB.Models
{
    public class Customer
    {
        public int ID { get; set; }

        [StringLength(60, MinimumLength = 2, ErrorMessage = "First name must be at least 2 characters long.")]
        [Required(ErrorMessage = "Please enter your first name.", AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [StringLength(60, MinimumLength = 2, ErrorMessage = "Last name must be at least 2 characters long.")]
        [Required(ErrorMessage = "Please enter your last name.", AllowEmptyStrings = false)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your date of birth.", AllowEmptyStrings = false)]
        [DataType(DataType.Date)]
        [DOBCheck(18, ErrorMessage = "You must be over 18 years of age to register.")]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please enter an email address.", AllowEmptyStrings = false)]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$",
            ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        [StringLength(100, MinimumLength = 4, ErrorMessage = "Password must be at least 4 characters long.")]
        [Required(ErrorMessage = "Please enter a password.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm your password.", AllowEmptyStrings = false)]
        [Compare("Password", ErrorMessage = "The provided passwords do not match.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }   
}