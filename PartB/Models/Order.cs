﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PartB.Models
{
    public class Order
    {
        public Order() 
        {
            itemList = new List<OrderItem>();
        }

        [Key]
        public int orderID { get; set; }
        public int customerID { get; set; }
        public List<OrderItem> itemList { get; private set; }
        public decimal totalPrice { get; set; }

        [Required(ErrorMessage = "Please enter your full name.", AllowEmptyStrings = false)]
        public string fullName { get; set; }

        [Display(Name = "Order Date")]
        public DateTime orderDate { get; set; }

        [StringLength(60, MinimumLength = 8, ErrorMessage = "Street Address must be at least 8 characters long.")]
        [Required(ErrorMessage = "Please enter your street address.", AllowEmptyStrings = false)]
        public string streetAddress { get; set; }

        [Required(ErrorMessage = "Please enter your city.", AllowEmptyStrings = false)]
        public string city { get; set; }

        [Required(ErrorMessage = "Please enter your state.", AllowEmptyStrings = false)]
        public string state { get; set; }

        [RegularExpression(@"^(\d{4})$", ErrorMessage = "Please enter a valid 4 digit postcode.")]
        [Required(ErrorMessage = "Please enter your postcode.", AllowEmptyStrings = false)]
        public int postcode { get; set; }
        
    }
}