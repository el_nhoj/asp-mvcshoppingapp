﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PartB.Models
{
    public class OrderItem
    {
        public OrderItem()
        {

        }

        [Key]
        public int orderItemID { get; set; }
        public int orderID { get; set; }
        public string productName { get; set; }
        public int productQty { get; set; }
        public decimal productSubtotal { get; set; }
    }
     
}