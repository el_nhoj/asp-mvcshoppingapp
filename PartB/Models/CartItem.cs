﻿using System.ComponentModel.DataAnnotations;

namespace PartB.Models
{
    public class CartItem
    {
        public CartItem(Product p, int qty) 
        {
            this.product = p;
            this.quantity = qty;
        }

        public Product product { get; set; }
        [RangeAttribute(1, int.MaxValue, ErrorMessage = "Product quantity must be at least 1.")]
        public int quantity { get; set; }
        public decimal subtotal { get; set; }
    }
}