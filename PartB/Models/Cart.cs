﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PartB.Models
{
    public class Cart
    {
        public Cart()
        {
            cartItems = new List<CartItem>();
        }

        public List<CartItem> cartItems { get; private set; }
        public int customerID { get; set; }
        [Display(Name = "Total Price")]
        public decimal total { get; set; }
    }
}