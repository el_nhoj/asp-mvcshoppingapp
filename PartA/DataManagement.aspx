﻿<%@ Page Language="C#" Title="Data Management Control Panel" MasterPageFile="MasterPage.Master" AutoEventWireup="true" CodeBehind="DataManagement.aspx.cs" Inherits="PartA.DataManagement" %>

<asp:Content ID="DataManagement" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <h3>Data Management Control Panel</h3>
    <div class="left">
        <p>Select Category:
        <asp:DropDownList ID="ddlCategory" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" 
            style="height: 22px" AutoPostBack="True">
        </asp:DropDownList>
        &nbsp;
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click1" CausesValidation="False" />
        &nbsp;</p>
    </div>    
    <div class="right">
        <p>Delete Category?
            &nbsp;
            <asp:Button ID="btnDeleteCategory" runat="server" OnClick="btnDeleteCategory_Click" Text="Delete" CausesValidation="False" OnClientClick="return confirm('Are you sure you want to delete this category?');" />
        </p>
    </div>
    <div class="clear"></div>
    <p><asp:Label ID="lblSystemMsg" runat="server"></asp:Label>
        <asp:ValidationSummary ID="vsEdit" runat="server" ValidationGroup="validateEdit" CssClass="error"/>
    <asp:Panel ID="pnlGrid" runat="server">
        <asp:GridView ID="GridView1" runat="server" 
            AutoGenerateColumns="False" Width="1000px" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="Product ID" ReadOnly="true"/>
                <asp:TemplateField HeaderText=" Product Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditPName" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEditPName" runat="server" ControlToValidate="txtEditPName" CssClass="error" ErrorMessage="Product Name cannot be empty." ValidationGroup="validateEdit">&nbsp;</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Short Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditSDesc" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:TextBox>
                        <br />
                        <br />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("ShortDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Long Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditLDesc" Rows="10" TextMode="MultiLine" runat="server" Text='<%# Bind("LongDescription") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("LongDescription") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Image">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditImgUrl" runat="server" Text='<%# Bind("ImageUrl") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEditIUrl" runat="server" ControlToValidate="txtEditImgUrl" CssClass="error" ErrorMessage="Image URL cannot be empty." ValidationGroup="validateEdit">&nbsp;</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("ImageUrl") %>' Height="100px" Width="100px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Price">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEditPrice" runat="server" Text='<%# Bind("Price", "{0:f2}") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEditPrice" runat="server" ControlToValidate="txtEditPrice" CssClass="error" ErrorMessage="Price cannot be empty." ValidationGroup="validateEdit">&nbsp;</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revEditPrice" runat="server" ControlToValidate="txtEditPrice" CssClass="error" ErrorMessage="Price is an invalid value." ValidationExpression="\d*\.\d{1,2}|\d+" ValidationGroup="validateEdit">&nbsp;</asp:RegularExpressionValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Price", "{0:f2}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ValidationGroup="validateEdit" CausesValidation="true"/>
            </Columns>
        </asp:GridView>          
    </asp:Panel>
    <br />
    <div class="left">
        <asp:Panel ID="pnlAddNew" runat="server" Visible="false">
        <h3>Add New Product Entry</h3>
        <p><asp:Label ID="lblAddMessage" runat="server" CssClass="success"></asp:Label>
            <asp:ValidationSummary ID="vsAddNew" runat="server" CssClass="error" DisplayMode="BulletList" EnableClientScript="true" ShowMessageBox="false" ShowSummary="true" ValidationGroup="validateAdd" />
            <table id="tblAddNew">
                <tr>
                    <th>Product Name<asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="txtProductName" CssClass="error" ErrorMessage="Product Name cannot be empty." ValidationGroup="validateAdd">*</asp:RequiredFieldValidator>
                    </th>
                    <td>
                        <asp:TextBox ID="txtProductName" runat="server" Width="230px" />
                    </td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>
                        <asp:TextBox ID="txtCategory" runat="server" ReadOnly="true" Width="230px" />
                    </td>
                </tr>
                <tr>
                    <th>Short Description</th>
                    <td>
                        <asp:TextBox ID="txtShortDesc" runat="server" Width="230px" />
                    </td>
                </tr>
                <tr>
                    <th>Long Description</th>
                    <td>
                        <asp:TextBox ID="txtLongDesc" runat="server" Rows="10" TextMode="multiline" Width="228px" />
                    </td>
                </tr>
                <tr>
                    <th>Price<asp:RequiredFieldValidator ID="rfvPrice" runat="server" ControlToValidate="txtPrice" CssClass="error" ErrorMessage="Price field cannot be empty." ValidationGroup="validateAdd">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexPrice" runat="server" ControlToValidate="txtPrice" CssClass="error" ErrorMessage="Price is an invalid value." ValidationExpression="\d*\.\d{1,2}|\d+" ValidationGroup="validateAdd">*</asp:RegularExpressionValidator>
                    </th>
                    <td>
                        <asp:TextBox ID="txtPrice" runat="server" Width="230px" />
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <asp:Button ID="btnAddNew" runat="server" OnClick="btnAddNew_Click" text="Add New" ValidationGroup="validateAdd" />
                    </td>
                </tr>
            </table>
            </p>
        </asp:Panel>
    </div>
    <div class="right">
        <asp:Panel ID="pnlUpload" runat="server" Visible="false">
        <h3>Upload an Image</h3>
        <p><a href="UploadImage.aspx" rel="next"><strong>Image Upload Manager &raquo;</strong></a></p>
        </asp:Panel>
    </div>
    <div class="clear"></div>        
</asp:Content>
