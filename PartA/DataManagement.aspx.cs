﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace PartA
{
    public partial class DataManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnDeleteCategory.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this category?');");
                BindDataToDDL();
            }            
        }

        // Method to arbitrarily return a connection string, used for sharing between group member servers
        private string ReturnConnectString()
        {
            return "server=john-pc;Integrated Security=True;database=s3401071";
        }

        // Method to bind category data to drop down list
        private void BindDataToDDL()
        {
            DataTable dataTable = new DataTable();

            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDisplayCategory'
                SqlCommand cmd = new SqlCommand("spDisplayCategory", connect);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandType = CommandType.StoredProcedure;

                da.Fill(dataTable);

                // If rows in the table exist, populate dropdown list
                if (dataTable.Rows.Count > 0)
                {
                    ddlCategory.DataSource = dataTable;
                    ddlCategory.DataTextField = "Title";
                    ddlCategory.DataValueField = "Title";
                    ddlCategory.DataBind();
                    lblSystemMsg.Text = "";
                }
                else
                {
                    ddlCategory.Items.Clear();
                }
            }
        }

        // Method that takes a category's ID and bind its related products to gridview
        private void BindDataToGrid(int catID)
        {
            DataTable dataTable = new DataTable();

            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDisplayProductsByCID'
                SqlCommand cmd = new SqlCommand("spDisplayProductsByCID", connect);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CategoryID", catID);

                da.Fill(dataTable);

                // If rows in the table exist, populate gridview, show data entry panels and return number of records
                if (dataTable.Rows.Count > 0)
                {
                    GridView1.DataSource = dataTable;
                    GridView1.DataBind();
                    pnlAddNew.Visible = true;
                    pnlUpload.Visible = true;
                    lblSystemMsg.Text = dataTable.Rows.Count + " record(s) found.";
                }
            }
        }

        // Method to add a product entry to the database
        private void AddProduct(string pName, int catID, string sDesc, string lDesc, string price)
        {
            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spAddProduct'
                SqlCommand cmd = new SqlCommand("spAddProduct", connect);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Title", pName);
                cmd.Parameters.AddWithValue("@CategoryID", catID);
                cmd.Parameters.AddWithValue("@ShortDescription", sDesc);
                cmd.Parameters.AddWithValue("@LongDescription", lDesc);
                cmd.Parameters.AddWithValue("@Price", price);

                cmd.ExecuteNonQuery();
            }
        }

        // Method to update a product entry from the database
        private void UpdateProduct(string pID, string pName, int catID, string sDesc, 
            string lDesc, string imgUrl, string price)
        {
            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spUpdateProduct'
                SqlCommand cmd = new SqlCommand("spUpdateProduct", connect);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ProductID", pID);
                cmd.Parameters.AddWithValue("@Title", pName);
                cmd.Parameters.AddWithValue("@CategoryID", catID);
                cmd.Parameters.AddWithValue("@ShortDescription", sDesc);
                cmd.Parameters.AddWithValue("@LongDescription", lDesc);
                cmd.Parameters.AddWithValue("@ImageURL", imgUrl);
                cmd.Parameters.AddWithValue("@Price", price);

                cmd.ExecuteNonQuery();
            }
        }

        // Method to delete a product's entry
        private void DeleteProduct(string pID)
        {
            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDeleteProductByPID'
                SqlCommand cmd = new SqlCommand("spDeleteProductByPID", connect);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ProductID", pID);

                cmd.ExecuteNonQuery();
            }          
        }

        // Method that accepts a string paraemeter and returns a category ID based on what the string is 
        private int ReturnCategoryID(string category) 
        {
            int ddlValue = 0;

            switch (category)
            {
                case "cpus":
                    ddlValue = 1;
                    break;
                case "memory":
                    ddlValue = 2;
                    break;
                case "graphics cards":
                    ddlValue = 3;
                    break;
            }
            return ddlValue;
        }

        // Method to delete a category
        private void DeleteCategory(int catID)
        {
            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDeleteCategory'
                SqlCommand cmd = new SqlCommand("spDeleteCategory", connect);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CategoryID", catID);

                cmd.ExecuteNonQuery();

                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            // If dropdown list value equals a case, change to CategoryID value and 
            // use as a parameter for BindDataToGrid method
            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());

            BindDataToGrid(ddlValue);

            // Pre-fill txtCategory textbox with ddlCategory's value and clear Add Entry message
            txtCategory.Text = ddlCategory.SelectedValue.ToString();
            lblAddMessage.Text = "";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int categoryValue = ReturnCategoryID(txtCategory.Text.ToLower());

                // If textboxes are valid, add new product entry and refresh grid to reflect change
                AddProduct(txtProductName.Text, categoryValue, txtShortDesc.Text, txtLongDesc.Text, txtPrice.Text);
                BindDataToGrid(categoryValue);

                lblAddMessage.Text = "New entry '" + txtProductName.Text + "' added successfully!";

                // Clears all related text fields
                txtProductName.Text = "";
                txtShortDesc.Text = "";
                txtLongDesc.Text = "";
                txtPrice.Text = "";
            }
        }
        
        protected void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());
            
            DeleteCategory(ddlValue);
            BindDataToDDL();
            
            // Display successful delete message and hide all panels
            lblSystemMsg.Text = "Category successfully deleted.";
            pnlAddNew.Visible = false;
            pnlUpload.Visible = false;
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            // Turn on GridView edit mode
            GridView1.EditIndex = e.NewEditIndex;

            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());

            // Refresh grid to reflect changes in category's data
            BindDataToGrid(ddlValue); 
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            // Cancel GridView edit mode
            GridView1.EditIndex = -1;

            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());
           
            // Refresh grid
            BindDataToGrid(ddlValue);  
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            // Get GridView's edit mode textbox values
            string pID = GridView1.Rows[e.RowIndex].Cells[0].Text;
            TextBox txtEditPName = GridView1.Rows[e.RowIndex].FindControl("txtEditPName") as TextBox;
            TextBox txtEditSDesc = GridView1.Rows[e.RowIndex].FindControl("txtEditSDesc") as TextBox;
            TextBox txtEditLDesc = GridView1.Rows[e.RowIndex].FindControl("txtEditLDesc") as TextBox;
            TextBox txtEditImgUrl = GridView1.Rows[e.RowIndex].FindControl("txtEditImgUrl") as TextBox;
            TextBox txtEditPrice = GridView1.Rows[e.RowIndex].FindControl("txtEditPrice") as TextBox;

            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());
            
            // Update values and exit GridView edit mode
            UpdateProduct(pID, txtEditPName.Text, ddlValue, txtEditSDesc.Text, txtEditLDesc.Text, 
                txtEditImgUrl.Text, txtEditPrice.Text);
            GridView1.EditIndex = -1;

            // Refresh grid to reflect changes in category's data
            BindDataToGrid(ddlValue);  
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Get product's ID from GridView column, deletes entry and refreshes grid
            string productID = GridView1.Rows[e.RowIndex].Cells[0].Text;
            
            DeleteProduct(productID);

            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());
            
            BindDataToGrid(ddlValue);
        }   
    }
}