﻿<%@ Page Language="C#" Title="Image Upload Manager" MasterPageFile="MasterPage.Master" AutoEventWireup="true" CodeBehind="UploadImage.aspx.cs" Inherits="PartA.UploadImage" %>

<asp:Content ID="DataManagement" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <div>
        <h3>Image Upload Manager</h3>            
        <p>Select a Category: 
            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
            </asp:DropDownList>
            &nbsp;</p>           
        <p>Select a Product: 
            <asp:DropDownList ID="ddlProduct" runat="server">
            </asp:DropDownList>
        </p>
        <asp:FileUpload ID="fileUpload" runat="server" />                     
        &nbsp;<br />
        <asp:RegularExpressionValidator ID="revUpload" runat="server" ControlToValidate="fileUpload" CssClass="error" ErrorMessage="File must be a JPG/JPEG, GIF or PNG file!" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpg|.JPG|.jpeg|.JPEG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
        <br />
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        <br />
        <p>
            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" Height="26px" Width="60px" />
            <br />
        </p>
        <br />
        <p><a href="DataManagement.aspx" rel="prev"><strong>Return to Data Management Control Panel &raquo;</strong></a></p>
    </div>      
</asp:Content>
