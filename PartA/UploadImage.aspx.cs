﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace PartA
{
    public partial class UploadImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindToDDLCategory();

                int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());

                BindDataToDDLProduct(ddlValue);
            }
        }

        private string ReturnConnectString()
        {
            return "server=john-pc;Integrated Security=True;database=s3401071";
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                // upload images into the database
                string str = fileUpload.FileName;
                string path = "~\\Images\\" + str.ToString();

                fileUpload.PostedFile.SaveAs(Server.MapPath(".") + "\\Images\\" + str);

                // start connection
                using (SqlConnection connect = new SqlConnection(ReturnConnectString()))
                {
                    connect.Open();

                    // Call stored procedure 'spInsertImageUrl'
                    SqlCommand cmd = new SqlCommand("spInsertImageUrl", connect);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@ImageUrl", path);
                    cmd.Parameters.AddWithValue("@Title", ddlProduct.SelectedValue.ToString());
                    cmd.ExecuteNonQuery();

                    // display success
                    lblMessage.Text = "Image '" + str + "' has been successfully uploaded!";
                }
            }
            else
            {
                // no image selected
                lblMessage.Text = "Please select an image to upload.";
            }
            revUpload.IsValid = true;
        }

        // Method to bind product name to drop down list
        private void BindDataToDDLProduct(int catID)
        {
            DataTable dataTable = new DataTable();

            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDisplayCategory'
                SqlCommand cmd = new SqlCommand("spDisplayProductTitle", connect);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CategoryID", catID);

                da.Fill(dataTable);

                // If rows in the table exist, populate dropdown list
                if (dataTable.Rows.Count > 0)
                {
                    ddlProduct.DataSource = dataTable;
                    ddlProduct.DataTextField = "Title";
                    ddlProduct.DataValueField = "Title";
                    ddlProduct.DataBind();
                }
                else
                {
                    ddlProduct.Items.Clear();
                }
            }
        }

        // Method that accepts a string paraemeter and returns a category ID based on what the string is 
        private int ReturnCategoryID(string category)
        {
            int ddlValue = 0;

            switch (category)
            {
                case "cpus":
                    ddlValue = 1;
                    break;
                case "memory":
                    ddlValue = 2;
                    break;
                case "graphics cards":
                    ddlValue = 3;
                    break;
            }
            return ddlValue;
        }

        // Bind category name to first dropdownlist 
        private void BindToDDLCategory() 
        {
            DataTable dataTable = new DataTable();

            using (SqlConnection connect = new SqlConnection(ReturnConnectString())) 
            {
                connect.Open();

                // Call stored procedure 'spDisplayCategory'
                SqlCommand cmd = new SqlCommand("spDisplayCategory", connect);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandType = CommandType.StoredProcedure;

                da.Fill(dataTable);

                // If rows in the table exist, populate dropdown list
                if (dataTable.Rows.Count > 0)
                {
                    ddlCategory.DataSource = dataTable;
                    ddlCategory.DataTextField = "Title";
                    ddlCategory.DataValueField = "Title";
                    ddlCategory.DataBind();
                }
                else
                {
                    ddlCategory.Items.Clear();
                }
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ddlValue = ReturnCategoryID(ddlCategory.SelectedValue.ToLower());
            BindDataToDDLProduct(ddlValue);
        }
    }
}
