===========================================
COSC2276/2277 Web Development Technologies
Assignment 2 - README.txt 	
===========================================
---------------
Group Members:
---------------
John Le (s3401071)
Michael Abanilla (s3323163)  

-------------
Description:
-------------
A shopping cart web application written in C#, using the Microsoft .NET ASP/MVC framework.

------------
References:
------------
Code references can be found in the project files.

Plugins
--------
'jquery.payment' - Credit Card validation plugin
https://github.com/stripe/jquery.payment

Fonts:
--------
'Press Start 2P'
http://www.dafont.com/press-start-2p.font

Images:
--------
- Part B
'Congruent Pentagon Outline' (Tiled Background)
http://subtlepatterns.com/congruent-pentagon-outline/

'Windows Hardware' (Jumbotron Background Image)
http://www.fantom-xp.com/wallpapers/19/Windows_Hardware.jpg